from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from receipts.forms import ReceiptForm, ExpenceCategoryForm
from receipts.forms import AccountForm
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required
def show_receipt(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list
    }
    return render(request, "receipts/list_receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)


@login_required
def show_expenses(request):
    categories = ExpenseCategory.objects.all()
    context = {
        "category_list": categories
    }
    return render(request, "receipts/list_categories.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenceCategoryForm(request.POST)
        if form.is_valid():
            category =  form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenceCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def show_accounts(request):
    accounts = Account.objects.all()
    context = {
        "account_list": accounts
    }
    return render(request, "receipts/list_accounts.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account =  form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
