from expenses.urls import path
from receipts.views import show_receipt, create_receipt, show_accounts
from receipts.views import show_expenses, create_category, create_account
urlpatterns = [
    path("", show_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_expenses, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", show_accounts, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
]
